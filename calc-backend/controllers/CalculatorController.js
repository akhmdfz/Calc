const Calculator = require("../models/Calculator");

exports.listAllCalcs = (req, res) => {
  Calculator.find({}, (err, calculator) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(calculator);
  });
};

exports.createNewCalc = (req, res) => {
  let newCalc = new Calculator(req.body);
  newCalc.save((err, calculator) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(201).json(calculator);
  });
};

exports.readCalc = (req, res) => {
  Calculator.findById(req.params.calcid, (err, calculator) => {
    if (err) {
      res.status(500).send(err);
    }
    res.status(200).json(calculator);
  });
};

exports.updateCalc = (req, res) => {
  Calculator.findOneAndUpdate(
    { _id: req.params.calcid },
    req.body,
    { new: true },
    (err, calculator) => {
      if (err) {
        res.status(500).send(err);
      }
      res.status(200).json(calculator);
    }
  );
};

exports.deleteCalc = (req, res) => {
  Calculator.remove({ }, (err, calculator) => {
    if (err) {
      res.status(404).send(err);
    }
    res.status(200).json({ message: "History successfully deleted" });
  });
};

exports.deleteCalcSingle = (req, res) => {
  Calculator.remove({ _id: req.params.calcid }, (err, calculator) => {
    if (err) {
      res.status(404).send(err);
    }
    res.status(200).json({ message: "Entry successfully deleted" });
  });
};