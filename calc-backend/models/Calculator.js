const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const CalcSchema = new Schema({
  calcHistory: {
    type: String,
    required: true
  },
  createdOn: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model("Calculator", CalcSchema);