const express = require("express");
const bodyParser = require("body-parser");
const calcController = require("./controllers/CalculatorController");

require("./config/db");

const app = express();

const port = process.env.PORT || 3301;
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

app
  .route("/calcs")
  .get(calcController.listAllCalcs)
  .post(calcController.createNewCalc)
  .delete(calcController.deleteCalc);

app
  .route("/calcs/:calcid")
  .get(calcController.readCalc)
  .put(calcController.updateCalc)
  .delete(calcController.deleteCalcSingle);
  
app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});