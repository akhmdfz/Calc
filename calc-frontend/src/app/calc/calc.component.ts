import { Component } from '@angular/core';
import { CalcService } from './calc.service';
import { MatSnackBarConfig, MatSnackBar } from '@angular/material';

@Component({
  selector: 'app-calc',
  templateUrl: './calc.component.html',
  styleUrls: ['./calc.component.scss'],
  host: {
    '(document:keypress)': 'handleKeyboardEvents($event)',
    '(document:keydown)': 'handleKeydownEvents($event)'
  }
})
export class CalcComponent {

    model: any = {};
    resultValue: string = '';
    resultValueExpression: boolean = false;
    clearValue: string = 'C'
    completePattern = new RegExp("[0-9-+*/^.()]");
    numbersPattern = new RegExp('^[0-9]+$');
    resultBackGroundColor: string = '#424242';
    parenthesisFlag: number = 0;
    calculation: string = '';
  
  constructor(private calcService: CalcService, private snackBar: MatSnackBar) { }

  handleKeydownEvents(keyDown: KeyboardEvent){
    let key = keyDown.key;
    switch (key) {
        case 'Delete' : this.clearButtonPress(); break;
        case 'Backspace' : this.clearButtonPress(); break;
    }
  }

  handleKeyboardEvents(keyPress: KeyboardEvent) {
    let key = keyPress.key;
    switch (key) {
        case '0': this.btnPress(key); break;
        case '1': this.btnPress(key); break;
        case '2': this.btnPress(key); break;
        case '3': this.btnPress(key); break;
        case '4': this.btnPress(key); break;
        case '5': this.btnPress(key); break;
        case '6': this.btnPress(key); break;
        case '7': this.btnPress(key); break;
        case '8': this.btnPress(key); break;
        case '9': this.btnPress(key); break;
        case '+': this.btnPress(key); break;
        case '-': this.btnPress(key); break;
        case '*': this.btnPress(key); break;
        case '/': this.btnPress(key); break;
        case '^': this.btnPress(key); break;
        case '(': this.btnPress(key); break;
        case ')': this.btnPress(key); break;
        case '.': this.btnPress(key); break;
        case '=': this.btnPress(key); break;
        case 'Enter': this.btnPress('='); break;
    }
  }

  btnPress(inputValue: string) {
    if (this.resultValue === 'Error' || this.resultValue === 'NaN') {this.resultValue = '';}
    if (this.validateInput(inputValue)) {
        if('/*-+'.indexOf(inputValue) === -1 && !this.resultValueExpression) {
            this.resultValue = inputValue;
        } else if(this.completePattern.test(inputValue)) {
            this.resultValue += inputValue;
        } else if(inputValue === '=') {
            this.evaluate();
            return;
        }            
        this.switchClearButton(true);
    } else {
        this.flashResult();
    }
  }

  validateInput(input) {
    let lastValue = this.resultValue.substr(this.resultValue.length - 1, 1);
    this.editParenthesisFlag(input, true);
    if (!lastValue || '(^/*+.'.indexOf(lastValue) > -1) {
        if (')*/^'.indexOf(input) > -1) {
            this.editParenthesisFlag(input, false);
            return false;
        }
        if (lastValue === '.' && ')(-+'.indexOf(input) > -1) {
            this.editParenthesisFlag(input, false);
            return false;
        }
    }
    if (this.numbersPattern.test(lastValue) && this.parenthesisFlag) {
        if (')'.indexOf(input) > -1) {
            this.editParenthesisFlag(input, false);
            return false;
        }
    }
    return true;
  }

  editParenthesisFlag(input: string, add: boolean) {
    if (add) {
        if ('('.indexOf(input) > -1) this.parenthesisFlag += 1;
        if (')'.indexOf(input) > -1) this.parenthesisFlag -= 1;
    } else {
        if ('('.indexOf(input) > -1) this.parenthesisFlag -= 1;
        if (')'.indexOf(input) > -1) this.parenthesisFlag += 1;
    }
  }

  flashResult() {
    let currentResultValue = this.resultValue;
    this.resultValue = 'Input Error';
    this.resultBackGroundColor = '#a02626';
    setTimeout(() => {
        this.resultBackGroundColor = '#424242';
        this.resultValue = currentResultValue;
    }, 500);
  }

  clearButtonPress() {
    if (this.resultValueExpression) {
        let valueToRemove = this.resultValue.substring(this.resultValue.length, this.resultValue.length - 1);
        if ('()'.indexOf(valueToRemove) > -1) {
            this.editParenthesisFlag(valueToRemove, false);
        }
        this.resultValue = this.resultValue.slice(0, -1);
        if (!this.resultValue) this.switchClearButton(false);
        else this.switchClearButton(true);
    } else {
        this.resultValue = '';
        this.calculation = '';
        this.parenthesisFlag = 0;
        this.switchClearButton(false);
    }
  }

  switchClearButton(input: boolean) {
    this.resultValueExpression = input;
    this.clearValue = (this.resultValueExpression) ? '←' : 'C';
  }

  replaceBy(target: string, find: string, replace: string) {
    return target
        .split(find)
        .join(replace);
  }

  roundValue(input: string) {
    const dotLocation: number = input.toString().indexOf('.');
    let flagDigit: boolean = false;

    if (dotLocation > -1 && input.length > 15) {
        const afterDotString = input.substring(dotLocation, input.length);
        for (let i = 0; i < afterDotString.length; i++) {
            if (parseInt(afterDotString[i]) > 0) flagDigit = true;
            if (afterDotString[i] === '0' && flagDigit) {
                return input.substring(0, dotLocation + i);
            }
        }
    }
    return input;
  }

  evaluate() {
    let config = new MatSnackBarConfig();
    config.verticalPosition = "top";
    config.duration = 5000;
    let valueToEvaluate: string = this.resultValue;
    let polishNotation: string = '';
    let solution: string = '';

    if (this.completePattern.test(valueToEvaluate) && !this.parenthesisFlag) {
        polishNotation = this.convertToPolishNotation(valueToEvaluate);
        solution = this.solvePolishNotation(polishNotation);
        this.calculation = this.resultValue + '';
        this.resultValue = this.roundValue(solution);
        this.model.calcHistory = JSON.stringify(this.calculation + '=' + this.resultValue);
        this.model.calcHistory = this.model.calcHistory.replace(/\"/g, "")
        this.calcService.save(this.model).subscribe(
            data =>{
            }, error => {
                this.snackBar.open(JSON.stringify(error.statusText), 'Dismiss', config);
            }
        )
    } else {
        this.calculation = 'Cannot resolve: ' + this.resultValue;
        this.resultValue = "Error";
        this.parenthesisFlag = 0;
    }
    this.switchClearButton(false);
  }

  convertToPolishNotation(input) {
    let output: string = "";
    let operatorStack: Array<any> = [];
    const operators = {
        "^": { weight: 3 },
        "/": { weight: 3 },
        "*": { weight: 3 },
        "+": { weight: 2 },
        "-": { weight: 2 }
    }

    input = this.standardizeString(input);
    input = input.replace(/\s+/g, '');
    input = input.split(/([\+\-\*\/\^\(\)])/);
    input = this.cleanArray(input);

    for (let i = 0; i < input.length; i++) {
        let token = input[i];
        if ('(^/*'.indexOf(input[i-1]) > -1 && token === '-') {
            token += input[i + 1];
            i += 1;
        }
        if (this.isNumeric(token)) {
            output += token + ' ';
        } else if ('*/^+-'.indexOf(token) !== -1) {
            let r1 = token;
            let r2 = operatorStack[operatorStack.length - 1];
            while ('*/^+-'.indexOf(r2) !== -1 && operators[r1].weight <= operators[r2].weight) {
                output += operatorStack.pop() + ' ';
                r2 = operatorStack[operatorStack.length - 1];
            }
            operatorStack.push(r1);
        } else if (token === '(') {
            operatorStack.push(token);
        } else if (token === ')') {
            while (operatorStack[operatorStack.length - 1] !== '(') {
                output += operatorStack.pop() + ' ';
            }
            operatorStack.pop();
        }
    }
    while (operatorStack.length > 0) {
        output += operatorStack.pop() + ' ';
    }
    return output;
}

solvePolishNotation(polish) {
    let results: Array<any> = [];
    polish = polish.split(" ");
    polish = this.cleanArray(polish);
    for (let i = 0; i < polish.length; i++) {
        if (this.isNumeric(polish[i])) {
            results.push(polish[i]);
        } else {
            let a = results.pop();
            let b = results.pop();
            if (polish[i] === '+') {
                results.push(parseFloat(a) + parseFloat(b));
            } else if (polish[i] === '-') {
                results.push(parseFloat(b) - parseFloat(a));
            } else if (polish[i] === '*') {
                results.push(parseFloat(a) * parseFloat(b));
            } else if (polish[i] === '/') {
                results.push(parseFloat(b) / parseFloat(a));
            }else if (polish[i] === '^') {
                results.push(Math.pow(a, b));
            }
        }
    }
    if (results.length > 1) {
        return 'Error';
    } else {
        return results
            .pop()
            .toString();
    }
  }

  standardizeString(input: string) {
    while (input.charAt(0) === '+') input = input.substr(1);
    input = this.replaceBy(input, ' ', '');
    input = this.replaceBy(input, 'x', '*');
    input = this.replaceBy(input, '-(', '-1*(');
    input = this.replaceBy(input, ')(', ')*(');
    input = this.replaceBy(input, '--', '+');
    input = this.replaceBy(input, '+-', '-');
    input = this.replaceBy(input, '-+', '-');
    input = this.replaceBy(input, '++', '+');
    input = this.replaceBy(input, '(+', '(');
    for (let i = 0; i < 10; i++) {
        input = this.replaceBy(input, `${i}(`, `${i}*(`);
    }
    return input;
  }

  cleanArray(input: Array<any>) {
    for (let i = 0; i < input.length; i++) {
        if (input[i] === '') input.splice(i, 1);
    }
    return input;
  }

  isNumeric(input: string) {
    return !isNaN(parseFloat(input));
  }
}
