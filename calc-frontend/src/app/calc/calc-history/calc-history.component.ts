import { Component, OnInit } from '@angular/core';
import { CalcService } from '../calc.service';
import { ISubscription } from 'rxjs/Subscription';
import { MatSnackBar, MatSnackBarConfig, MatDialog } from '@angular/material';
import { CalcEditComponent } from './calc-edit.component';

@Component({
  selector: 'app-calc-history',
  templateUrl: './calc-history.component.html',
  styleUrls: ['../calc.component.scss']
})
export class CalcHistoryComponent implements OnInit {

  position: string = 'above';
  subscription: ISubscription;
  itemList: any;
  constructor(
    private calcService: CalcService, 
    public snackBar: MatSnackBar,
    private dialog: MatDialog
  ) { }

  ngOnInit() {
    localStorage.setItem('reload','1');
    this.subscription = this.calcService.getHistory().subscribe(
      data=>{
        this.itemList = data;
      }, error => [
        console.log(error)
      ]
    )
  }

  ngDoCheck(){
    if (localStorage.getItem('reload') === '2'){
      this.ngOnInit();
      localStorage.setItem('reload','1');
    }
  }

  ngOnDestroy(): void{
    this.subscription.unsubscribe();
  }

  clear(){
    let config = new MatSnackBarConfig();
    config.verticalPosition = "top";
    config.duration = 5000;
    if(confirm("Are you sure want to clear entries?")){
    this.calcService.clear().subscribe(
      data=>{
        this.snackBar.open(JSON.stringify(data.message), 'Dismiss', config);
        localStorage.setItem('reload','2');
      }, error =>{
        this.snackBar.open(JSON.stringify(error.statusText), 'Dismiss', config);
      }
    )
  }
  }

  openDialogEdit(items: any) {
    const dialogRef = this.dialog.open(CalcEditComponent,
      {
        data: items,
        width: '420px',
      }
    );
  }
}
