import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CalcHistoryComponent } from './calc-history.component';

describe('CalcHistoryComponent', () => {
  let component: CalcHistoryComponent;
  let fixture: ComponentFixture<CalcHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CalcHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CalcHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
