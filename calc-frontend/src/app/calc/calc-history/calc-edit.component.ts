import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatSnackBar, MatSnackBarConfig, MAT_DIALOG_DATA } from '@angular/material';
import { CalcService } from '../calc.service';

@Component({
  selector: 'app-calc-edit',
  templateUrl: './calc-edit.component.html',
  styleUrls: ['../calc.component.scss']
})
export class CalcEditComponent implements OnInit {

  model: any = {};
  config = new MatSnackBarConfig();
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialog: MatDialog,
    private calcService: CalcService,
    private snackBar: MatSnackBar
  ) { }


  ngOnInit() {

    this.config.verticalPosition = "top";
    this.config.duration = 5000;

    this.calcService.get(this.data).subscribe()
    this.model.calcHistory = this.data.calcHistory
    this.model._id = this.data._id
  }

  save(){

    this.calcService.update(this.model).subscribe(
      data => {
        this.snackBar.open("Data has been updated", 'Dismiss', this.config);
        localStorage.setItem('reload','2');
        this.dialog.closeAll();
      }, error => {
        this.snackBar.open(JSON.stringify(error.statusText), 'Dismiss', this.config);
      }
    )
  }
  delete(){
    this.calcService.delete(this.model).subscribe(
      data => {
        this.snackBar.open(JSON.stringify(data.message), 'Dismiss', this.config);
        localStorage.setItem('reload','2');
        this.dialog.closeAll();
      }, error => {
        this.snackBar.open(JSON.stringify(error.statusText), 'Dismiss', this.config);
      }
    )
  }
}
