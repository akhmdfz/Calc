import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { HttpClient,HttpHeaders  } from '@angular/common/http';
import { AppConfig } from '../app.config';

@Injectable()
export class CalcService {

  constructor(private http: HttpClient, private appConfig: AppConfig) { }

  getHistory(): Observable<any>{
    return this.http.get<any>(this.appConfig.apiUrl + "/calcs");
  }

  save(items: any): Observable<any>{
    return this.http.post<any>(this.appConfig.apiUrl + "/calcs", items);
  }

  clear(): Observable<any>{
    return this.http.delete<any>(this.appConfig.apiUrl + "/calcs");
  }

  get(items: any): Observable<any>{
    return this.http.get<any>(this.appConfig.apiUrl + "/calcs/" + items._id);
  }

  update(items: any): Observable<any>{
    return this.http.put<any>(this.appConfig.apiUrl + "/calcs/" + items._id, items);
  }

  delete(items: any): Observable<any>{
    return this.http.delete<any>(this.appConfig.apiUrl + "/calcs/" + items._id);
  }
}
