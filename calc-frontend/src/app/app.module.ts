import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgProgressModule, NgProgressInterceptor } from 'ngx-progressbar';


import { AppComponent } from './app.component';
import { CalcComponent } from './calc/calc.component';
import { RouterModule } from '@angular/router';
import { AppRoutes } from './app.routing';

import * as mat from '@angular/material';
import { FormsModule } from '@angular/forms';
import { CalcHistoryComponent } from './calc/calc-history/calc-history.component';
import { CalcService } from './calc/calc.service';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppConfig } from './app.config';
import { CalcEditComponent } from './calc/calc-history/calc-edit.component';

@NgModule({
  declarations: [
    AppComponent,
    CalcComponent,
    CalcHistoryComponent,
    CalcEditComponent
  ],
  imports: [
    NgProgressModule,
    FormsModule,
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    BrowserAnimationsModule,
    HttpClientModule,
    mat.MatSnackBarModule,
    mat.MatCardModule,
    mat.MatFormFieldModule,
    mat.MatInputModule,
    mat.MatButtonModule,
    mat.MatGridListModule,
    mat.MatIconModule,
    mat.MatListModule,
    mat.MatTooltipModule,
    mat.MatDialogModule
  ],
  entryComponents: [
    CalcEditComponent
  ],
  providers: [
    CalcService,
    AppConfig,
    { provide: HTTP_INTERCEPTORS, useClass: NgProgressInterceptor, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
