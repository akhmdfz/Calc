import { Routes } from '@angular/router';
import { CalcComponent } from './calc/calc.component';
import { CalcHistoryComponent } from './calc/calc-history/calc-history.component';

export const AppRoutes: Routes = [

    {
      path: '',
      component: CalcComponent,
    },
    {
      path: 'history',
      component: CalcHistoryComponent
    }
];
